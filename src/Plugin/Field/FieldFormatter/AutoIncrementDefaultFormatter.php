<?php

namespace Drupal\auto_increment\Plugin\Field\FieldFormatter;

use Drupal\Core\Utility\Token;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'auto_increment_default_formatter'.
 *
 * @FieldFormatter(
 *   id = "auto_increment_default_formatter",
 *   label = @Translation("Auto increment default"),
 *   field_types = {
 *     "auto_increment"
 *   }
 * )
 */
class AutoIncrementDefaultFormatter extends FormatterBase {

  /**
   * Drupal\Core\Render\Renderer definition.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * Construct a MyFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, Renderer $renderer, Token $token_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
    $this->tokenService = $token_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $entity = $items->getEntity();
      $entityType = $entity->getEntityTypeId();

      // Prepare the token data context.
      $token_data = [
        $entityType => $entity, // Directly pass the entity.
      ];

      // Replace tokens in the field value.
      $replaced_value = $this->tokenService->replace(
        $item->value,   // The token string.
        $token_data,    // Context data for tokens.
        ['clear' => TRUE] // Remove unresolved tokens.
      );

      // Render output using auto_increment_default theme.
      $source = [
        '#theme' => 'auto_increment_default',
        '#auto_increment_id' => $replaced_value,
      ];
      $elements[$delta] = [
        '#markup' => DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.3.0', fn () => $this->renderer->renderInIsolation($source), fn () => $this->renderer->renderPlain($source),
        ),
      ];
    }
    return $elements;
  }

}
