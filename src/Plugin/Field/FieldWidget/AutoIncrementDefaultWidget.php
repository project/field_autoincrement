<?php

namespace Drupal\auto_increment\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'auto_increment_default' widget.
 *
 * @FieldWidget(
 *   id = "auto_increment_default_widget",
 *   label = @Translation("Hidden (Automatic)"),
 *   field_types = {
 *     "auto_increment"
 *   }
 * )
 */
class AutoIncrementDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $label = $items[$delta]->getFieldDefinition()->getLabel();
    $element['value'] = [
    // Understand number (integer)
      '#type' => 'hidden',
      '#title' => $label,
      // Default value cannot be NULL,
      // throws 'This value should be of the correct primitive type'.
      // @see https://www.drupal.org/node/2220381
      // so the auto is defaulted to a positive int.
      '#default_value' => $items[$delta]->value ?? '',
    ];
    return $element;
  }

}
