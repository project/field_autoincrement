<?php

namespace Drupal\auto_increment\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Plugin implementation of the 'auto_increment' field type.
 *
 * @todo should not be translatable, by default
 *
 * @FieldType(
 *   id = "auto_increment",
 *   label = @Translation("Auto increment"),
 *   description = @Translation("Auto increment field type."),
 *   default_widget = "auto_increment_default_widget",
 *   default_formatter = "auto_increment_default_formatter"
 * )
 */
class AutoIncrementItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'start_value' => 1,
      'init_existing_entities' => 0,
      'prefix_value' => '',
      'suffix_value' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    $element['start_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Starting value'),
      '#default_value' => $this->getSetting('start_value'),
      '#min' => 1,
      '#required' => TRUE,
      '#disabled' => $has_data,
    ];
    $element['init_existing_entities'] = [
      '#type' => 'radios',
      '#title' => $this->t('Start on existing entities'),
      '#description' => $this->t('When this field is created for a bundle that already have entities.'),
      '#options' => [0 => $this->t('No'), 1 => $this->t('Yes')],
      '#default_value' => $this->getSetting('init_existing_entities'),
      '#required' => TRUE,
      '#disabled' => $has_data,
    ];
    $element['prefix_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add Prefix'),
      '#description' => $this->t('When this field is created using this value as a prefix.'),
      '#size' => 20,
      '#default_value' => $this->getSetting('prefix_value'),
      '#disabled' => $has_data,
    ];
    $element['suffix_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add Suffix'),
      '#description' => $this->t('When this field is created using this value as a suffix.'),
      '#size' => 20,
      '#default_value' => $this->getSetting('suffix_value'),
      '#disabled' => $has_data,
    ];
    $element['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => array('user', 'node'),
    ];
    // Only run the initialization when the field has no data.
    if (!$has_data) {
      // @todo ideally, use submit handler and not validate
      // $handlers = $form_state->getSubmitHandlers();
      // $handlers[] = [$this, 'initializeEntitiesCallback'];
      // $form_state->setSubmitHandlers($handlers);
      $form['#validate'][] = [$this, 'initializeEntitiesCallback'];
    }
    return $element;
  }

  /**
   * Initialize entities depending on the storage settings.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   */
  public function initializeEntitiesCallback(array &$form, FormStateInterface $form_state) {
    // Check if existing entities have to be initialized.
    $settings = $form_state->getValue('settings');
    if ((int) $settings['init_existing_entities'] === 1) {
      $startValue = (int) $settings['start_value'];
      $prefixValue = $settings['prefix_value'];
      $suffixValue = $settings['suffix_value'];
      // Check then first if the bundle has entities.
      $fieldConfig = $this->getFieldDefinition();
      $entityTypeId = $fieldConfig->getTargetEntityTypeId();
      $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
      $bundleKey = $storage->getEntityType()->getKey('bundle');
      $bundle = $fieldConfig->getTargetBundle();
      $query = \Drupal::entityQuery($entityTypeId);
      $query->condition($bundleKey, $bundle);
      $ids = $query->accessCheck(FALSE)->execute();

      if (count($ids) > 0) {
        /** @var \Drupal\auto_increment\AutoIncrementStorageInterface $autoIncrementStorage */
        $autoIncrementStorage = \Drupal::getContainer()->get('auto_increment.sql_storage');
        // Set autoIncrement values for existing entities.
        $oldCount = $autoIncrementStorage->initOldEntries(
          $entityTypeId,
          $bundle,
          $fieldConfig->getFieldStorageDefinition()->getName(),
          $startValue,
          $prefixValue,
          $suffixValue
        );
        if ($oldCount > 0) {
          \Drupal::messenger()->addMessage($this->t('AutoIncrement values have been automatically set for %count existing entities, starting from %start_value.', [
            '%count' => $oldCount,
            '%start_value' => $startValue,
          ]));
        }
      }
      else {
        \Drupal::messenger()->addWarning($this->t('No entities to initialize, the next entity to be created will start from %start_value.', [
          '%start_value' => $startValue,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // @todo review DataDefinition methods : setReadOnly, setComputed, setRequired, setConstraints
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $auto = NULL;
    $entity = $this->getEntity();

    $entityBundle = $this->getFieldDefinition()->getTargetBundle();
    // Get the starting value from the storage settings.
    $settings = $this->getSettings();
    $startValue = $settings['start_value'] ?? 1;
    $prefixValue = $settings['prefix_value'];
    $suffixValue = $settings['suffix_value'];
    $newAuto = FALSE;

    // Does not apply if the node is not new or translated.
    if ($entity->isNew()) {
      $newAuto = TRUE;
    }
    else {
      // Handle entity translation: fetch the same id or another one
      // depending of what is the design.
      // This should probably be solved by the end user decision
      // while setting the field translation.
      /** @var \Drupal\Core\Language\LanguageManagerInterface $languageManager */
      $languageManager = \Drupal::getContainer()->get('language_manager');
      // @todo isMultilingual is global, prefer local hasTranslation
      if ($languageManager->isMultilingual() && $entity instanceof TranslatableInterface) {
        $newAuto = $entity->isNewTranslation();
      }
    }

    if ($newAuto) {

      /** @var \Drupal\auto_increment\AutoIncrementStorageInterface $autoIncrementStorage */
      $autoIncrementStorage = \Drupal::getContainer()->get('auto_increment.sql_storage');
      $storageName = $autoIncrementStorage->createStorageNameFromField($this->getFieldDefinition(), $this->getEntity());

      $query = \Drupal::database()->select($storageName, 'ai');
      $query->addField('ai', 'value');
      $query->condition('ai.type', $entityBundle);
      $query->orderBy('value', 'DESC'); // Sort in descending order to get the highest value
      $query->range(0, 1); // Limit the result to one record
      $results = $query->execute()->fetchAll();

      if($results) {
        $preValue = (int) $results[0]->value;
        $auto = (int) $preValue + 1;
      } else {
        $auto = (int) $startValue;
      }

      $autoIncrementStorage->generateValueFromName($storageName, $entityBundle, $auto);
      // Subtract one as it is already added in code above.
      $this->setValue($prefixValue . $auto . $suffixValue);
    }
  }

}
