<?php

namespace Drupal\auto_increment;

use Drupal\Core\Utility\Error;
use Drupal\Core\Database\Database;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * AutoIncrement storage service definition.
 *
 * @todo before going further in other impl to AutoIncrementStorageInterface must be reviewed.
 *
 * SQL implementation for AutoIncrementStorage.
 */
class AutoIncrementSQLStorage implements ContainerInjectionInterface, AutoIncrementStorageInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The Drupal Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManager $entityFieldManager, LoggerChannelFactoryInterface $loggerFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'), $container->get('entity_field.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageNameFromField(FieldDefinitionInterface $fieldDefinition, FieldableEntityInterface $entity) {
    return $this->createStorageName(
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $fieldDefinition->getName()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageName($entityTypeId, $entityBundle, $fieldName) {
    // To make sure we don't end up with table names longer than 64 characters,
    // which is a MySQL limit we hash a combination of fields.
    // @todo Think about improvement for this.
    $tableName = 'auto_increment_' . "{$entityTypeId}_{$entityBundle}_{$fieldName}";
    return Database::getConnection()->escapeTable($tableName);
  }

  /**
   * {@inheritdoc}
   */
  public function generateValueFromName($storageName, $entityBundle, $startValue) {
    $connection = Database::getConnection();
    // @todo review https://api.drupal.org/api/drupal/core%21includes%21database.inc/function/db_transaction/8.2.x
    $transaction = $connection->startTransaction();

    try {

      $connection->insert($storageName)
        ->fields([
          'type' => $entityBundle,
          'value' => $startValue,
        ])
        ->execute();
    }
    // @todo use dedicated Exception
    // https://www.drupal.org/node/608166
    catch (\Exception $e) {
      $transaction->rollback();
      Error::logException($this->loggerFactory->get('auto_increment'), $e);
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateValue(FieldDefinitionInterface $fieldDefinition,
                                FieldableEntityInterface $entity) {
    $storageName = $this->createStorageNameFromField($fieldDefinition, $entity);
    $entityBundle = $fieldDefinition->getTargetBundle();
    $startValue = $fieldDefinition->getSetting('start_value') ?? 1;
    return $this->generateValueFromName($storageName, $entityBundle, $startValue);
  }

  /**
   * {@inheritdoc}
   */
  public function getSchema() {
    $schema = [
      'fields' => [
        'id' => [
          // Auto Increment Drupal DB type
          // means auto increment.
          // https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!database.api.php/group/schemaapi/8.2.x
          'type' => 'serial',
          'not null' => TRUE,
          'unsigned' => TRUE,
          'description' => 'Primary identifier.',
        ],
        'type' => [
          'type' => 'varchar',
          'length' => 23,
          'default' => '',
          'not null' => TRUE,
          // @todo review UUID instead
          'description' => 'Entity type / bundle.',
        ],
        'value' => [
          'type' => 'varchar',
          'length' => 23,
          'default' => '',
          'not null' => TRUE,
          // @todo review UUID instead
          'description' => 'Last entity saved value',
        ],
      ],
      'primary key' => ['id'],
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllFields() {
    return $this->entityFieldManager->getFieldMapByFieldType(AutoIncrementStorageInterface::AUTO_INCREMENT_FIELD_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function createStorage(FieldDefinitionInterface $fieldDefinition, FieldableEntityInterface $entity) {
    $storageName = $this->createStorageNameFromField($fieldDefinition, $entity);
    $this->createStorageFromName($storageName);
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageFromName($storageName) {
    $dbSchema = Database::getConnection()->schema();
    if (!$dbSchema->tableExists($storageName)) {
      $dbSchema->createTable($storageName, $this->getSchema());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function dropStorage(FieldDefinitionInterface $fieldDefinition, FieldableEntityInterface $entity) {
    $this->dropStorageFromName($this->createStorageNameFromField($fieldDefinition, $entity));
  }

  /**
   * {@inheritdoc}
   */
  public function dropStorageFromName($storageName) {
    $dbSchema = Database::getConnection()->schema();
    $dbSchema->dropTable($storageName);
  }

  /**
   * {@inheritdoc}
   */
  public function initOldEntries($entityTypeId, $entityBundle, $fieldName, $startValue, $prefixValue, $suffixValue) {
    $storage = $this->entityTypeManager->getStorage($entityTypeId);
    $query = $storage->getQuery();
    // @todo shall we assign auto_increment id to unpublished as well?
    // $query->condition('status', 1);
    $bundleKey = $storage->getEntityType()->getKey('bundle');
    $query->condition($bundleKey, $entityBundle);
    $query->accessCheck(FALSE);
    $entityIds = $query->execute();

    $updated = 0;
    $storageName = $this->createStorageName(
      $entityTypeId,
      $entityBundle,
      $fieldName
    );
    foreach ($entityIds as $entityId) {
      $updated = 0;
      $increment = 1;
      $entity = $storage->loadUnchanged($entityId);

      $query = \Drupal::database()->select($storageName, 'au');
      $query->addField('au', 'value');
      $query->condition('au.type', $entityBundle);
      $results = $query->execute()->fetchAll();

      if($results) {
        $preValue = (int) $results[0]->value;
        $newValue = $preValue + $increment;
      } else {
        $this->generateValueFromName($storageName, $entityBundle, $startValue);
        $newValue = $startValue;
      }

      // @todo review multilingual
      $entity->{$fieldName}->value = $prefixValue . $newValue . $suffixValue;
      if ($entity->save() === SAVED_UPDATED) {
        ++$updated;
        // update the current value
        $this->generateValueFromName($storageName, $entityBundle, $newValue);
      }
    }

    return $updated;
  }

}
