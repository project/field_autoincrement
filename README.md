# Auto Increment Field

This module provides an `Auto Increment` field type that automatically
increments the value with a given prefix and suffix.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

* Install the View Mode Switch Field module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.
